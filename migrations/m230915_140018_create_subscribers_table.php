<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subscribers}}`.
 */
class m230915_140018_create_subscribers_table extends Migration
{ /**
 * {@inheritdoc}
 */
    public function safeUp()
    {
        $this->createTable('subscribers', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'recipient_email' => $this->string()->notNull(),
            'blocked' => $this->boolean()->defaultValue(false),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
            'user_id' => $this->integer(), // Если вы хотите связать с пользователями
        ]);

        // Создайте индекс для внешнего ключа, связывающего 'event_id' с 'events.id'
        $this->createIndex(
            'idx-subscribers-event_id',
            'subscribers',
            'event_id'
        );

        // Добавьте внешний ключ, связывающий 'event_id' с 'events.id'
        $this->addForeignKey(
            'fk-subscribers-event_id',
            'subscribers',
            'event_id',
            'events',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // Удалите внешний ключ
        $this->dropForeignKey('fk-subscribers-event_id', 'subscribers');

        // Удалите индекс
        $this->dropIndex('idx-subscribers-event_id', 'subscribers');

        $this->dropTable('subscribers');
    }
}
