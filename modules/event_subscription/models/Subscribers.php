<?php

namespace app\modules\event_subscription\models;

use Yii;

/**
 * This is the model class for table "subscribers".
 *
 * @property int $id
 * @property int $event_id
 * @property string $recipient_email
 * @property int|null $blocked
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $user_id
 *
 * @property Events $event
 */
class Subscribers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscribers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'recipient_email'], 'required'],
            [['event_id', 'blocked', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['recipient_email'], 'string', 'max' => 255],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Events::class, 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Событие',
            'recipient_email' => 'Получатель',
            'blocked' => 'Заблокирован',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }
}
