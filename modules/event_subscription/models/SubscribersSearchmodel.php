<?php

namespace app\modules\event_subscription\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event_subscription\models\Subscribers;

/**
 * SubscribersSearchmodel represents the model behind the search form of `app\modules\event_subscription\models\Subscribers`.
 */
class SubscribersSearchmodel extends Subscribers
{
    public $event_name; // Новое свойство для поиска по названию события

    public function rules()
    {
        return [
            [['id', 'event_id', 'blocked'], 'integer'],
            [['recipient_email', 'created_at', 'updated_at', 'event_name'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Subscribers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'blocked' => $this->blocked,
        ]);

        $query->andFilterWhere(['like', 'recipient_email', $this->recipient_email])
            ->andFilterWhere(['like', 'event.name', $this->event_name]); // Фильтрация по названию события

        return $dataProvider;
    }
}
