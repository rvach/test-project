<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\event_subscription\SubscribersSearchmodel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список подписчиков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'event_id',
                'value' => function ($model) {
                    return $model->event->name; // Отображение названия события
                },
                'filter' => ArrayHelper::map($events, 'id', 'name'), // Фильтрация по событиям
            ],
            'recipient_email',
            'blocked:boolean',
            'created_at:datetime',
            'updated_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

