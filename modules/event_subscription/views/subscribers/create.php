<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $subscriber->isNewRecord ? 'Создать подписчика' : 'Редактировать подписчика #' . $subscriber->id;
$this->params['breadcrumbs'][] = ['label' => 'Список подписчиков', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-form">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($subscriber, 'event_id')->dropDownList(
        \yii\helpers\ArrayHelper::map($events, 'id', 'name'),
        ['prompt' => 'Выберите событие']
    ) ?>

    <?= $form->field($subscriber, 'recipient_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($subscriber, 'blocked')->dropDownList(['0' => 'Нет', '1' => 'Да']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
