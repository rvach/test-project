<?php

use yii\helpers\Html;

$this->title = 'Подписчик #' . $subscriber->id;
$this->params['breadcrumbs'][] = ['label' => 'Список подписчиков', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <td><?= $subscriber->id ?></td>
        </tr>
        <tr>
            <th>Событие</th>
            <td><?= $subscriber->event->name ?></td>
        </tr>
        <tr>
            <th>Получатель</th>
            <td><?= $subscriber->recipient_email ?></td>
        </tr>
        <tr>
            <th>Заблокирован</th>
            <td><?= $subscriber->blocked ? 'Да' : 'Нет' ?></td>
        </tr>
        <tr>
            <th>Дата добавления</th>
            <td><?= Yii::$app->formatter->asDatetime($subscriber->created_at) ?></td>
        </tr>
        <tr>
            <th>Дата редактирования</th>
            <td><?= Yii::$app->formatter->asDatetime($subscriber->updated_at) ?></td>
        </tr>
    </table>
</div>

