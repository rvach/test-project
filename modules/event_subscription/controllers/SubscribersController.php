<?php

namespace app\modules\event_subscription\controllers;

use app\modules\event_subscription\models\Subscribers;
use app\modules\event_subscription\models\SubscribersSearchmodel;
use app\modules\event_subscription\models\Events;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * SubscribersController implements the CRUD actions for Subscribers model.
 */
class SubscribersController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // Проверяем, авторизован ли пользователь
            if (Yii::$app->user->isGuest) {
                throw new ForbiddenHttpException('Доступ запрещен. Вы не авторизованы.');
            }
            return true;
        }
        return false;
    }

    public function actionIndex()
    {
        $searchModel = new SubscribersSearchmodel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $events = Events::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'events' => $events,
        ]);
    }

    public function actionView($id)
    {
        $subscriber = Subscribers::findOne($id);
        if (!$subscriber) {
            throw new NotFoundHttpException('Подписчик не найден.');
        }

        return $this->render('view', [
            'subscriber' => $subscriber,
        ]);
    }

    public function actionCreate()
    {
        $subscriber = new Subscribers();

        if ($subscriber->load(Yii::$app->request->post()) && $subscriber->save()) {
            Yii::$app->session->setFlash('success', 'Подписчик успешно создан.');
            return $this->redirect(['view', 'id' => $subscriber->id]);
        }

        $events = Events::find()->all();


        return $this->render('create', [
            'subscriber' => $subscriber,
            'events' => $events,
        ]);
    }

    public function actionUpdate($id)
    {
        $subscriber = Subscribers::findOne($id);
        if (!$subscriber) {
            throw new NotFoundHttpException('Подписчик не найден.');
        }

        if ($subscriber->load(Yii::$app->request->post()) && $subscriber->save()) {
            Yii::$app->session->setFlash('success', 'Подписчик успешно обновлен.');
            return $this->redirect(['view', 'id' => $subscriber->id]);
        }

        $events = Events::find()->all();

        return $this->render('update', [
            'subscriber' => $subscriber,
            'events' => $events,
        ]);
    }
}
